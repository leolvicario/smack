//
//  ChannelCell.swift
//  Smack
//
//  Created by Leonardo Luis Vicario on 10/5/18.
//  Copyright © 2018 Leonardo Luis Vicario. All rights reserved.
//

import UIKit

class ChannelCell: UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        if selected {
            self.layer.backgroundColor = UIColor(white: 1, alpha: 0.2).cgColor
        } else {
            self.layer.backgroundColor = UIColor.clear.cgColor
        }
    }
    
    func configureCell(channel: Channel) {
        name.text = "#\(channel.name ?? "")"
        name.font = UIFont(name: "HelveticaNeue-Regular", size: 17)
        
        for id in MessageService.instance.unreadChannels {
            if id == channel._id {
                name.font = UIFont(name: "HelveticaNeue-Bold", size: 22)
            }
        }
    }
    

}

//
//  MessageCell.swift
//  Smack
//
//  Created by Leonardo Luis Vicario on 16/5/18.
//  Copyright © 2018 Leonardo Luis Vicario. All rights reserved.
//

import UIKit

class MessageCell: UITableViewCell {

    @IBOutlet weak var userImg: CircleImage!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var timeStampLabel: UILabel!
    @IBOutlet weak var messageBodyText: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(message : Message) {
        userImg.image = UIImage(named: message.userAvatar)
        userImg.backgroundColor = UserDataService.instance.returnUIColor(components: message.userAvatarColor)
        userNameLabel.text = message.userName
        messageBodyText.text = message.message
        
        guard let isoDate = message.timestamp else {return}
        let end = isoDate.index(isoDate.endIndex, offsetBy: -5)
        let subIsoDate = isoDate[..<end]
        
        let isoFormatter = ISO8601DateFormatter()
        let chatDate = isoFormatter.date(from: subIsoDate.appending("Z"))
        
        let newFormatter = DateFormatter()
        newFormatter.dateFormat = "MMM d, h:mm a"
        
        if let finalDate = chatDate {
            let finalDate = newFormatter.string(from: finalDate)
            timeStampLabel.text = finalDate
        }
    }

}

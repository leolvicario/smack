//
//  MessageService.swift
//  Smack
//
//  Created by Leonardo Luis Vicario on 10/5/18.
//  Copyright © 2018 Leonardo Luis Vicario. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class MessageService {
    
    static let instance = MessageService()
    
    var channels = [Channel]()
    var messages = [Message]()
    var unreadChannels = [String]()
    var selectedChannel : Channel?
    
    func findAllChannels(completion: @escaping CompletionHandler) {
        Alamofire.request(Channel_Url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: Header_Bearer).responseJSON { (response) in
            
            if response.result.error == nil {
                guard let data = response.data else {return}
                do {
                    if let json = try JSON(data: data).array {
                        for item in json {
                            let id = item["_id"].stringValue
                            let name = item["name"].stringValue
                            let description = item["description"].stringValue
                            let version = item["__v"].intValue
                            let channel = Channel(_id: id, name: name, description: description, __v: version)
                            self.channels.append(channel)
                        }
                        NotificationCenter.default.post(name: Notif_Channels_Loaded, object: nil)
                        completion(true)
                    }
                } catch let error {
                    debugPrint(error as Any)
                }
            } else {
                completion(false)
                debugPrint(response.result.error as Any)
            }
        }
    }
    
    func findAllMessagesForChannel(channelId: String, completion: @escaping CompletionHandler) {
        Alamofire.request("\(Messages_Get_Url)\(channelId)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: Header_Bearer).responseJSON { (response) in
            if response.result.error == nil {
                self.clearMessages()
                guard let data = response.data else {return}
                do {
                    if let json = try JSON(data: data).array {
                        for item in json {
                            let messageBody = item["messageBody"].stringValue
                            let channelId = item["channelId"].stringValue
                            let id = item["_id"].stringValue
                            let userName = item["userName"].stringValue
                            let userAvatar = item["userAvatar"].stringValue
                            let userAvatarColor = item["userAvatarColor"].stringValue
                            let timestamp = item["timestamp"].stringValue
                            
                            let message = Message(message: messageBody, userName: userName, channelId: channelId, userAvatar: userAvatar, userAvatarColor: userAvatarColor, id: id, timestamp: timestamp)
                            self.messages.append(message)
                        }
                        completion(true)
                    }
                } catch let error {
                    debugPrint(error as Any)
                }
            } else {
                debugPrint(response.result.error as Any)
                completion(false)
            }
        }
    }
    
    func clearMessages() {
        messages.removeAll()
    }
    
    func clearChannels() {
        channels.removeAll()
    }
    
    
    
}

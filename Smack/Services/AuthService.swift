//
//  AuthService.swift
//  Smack
//
//  Created by Leonardo Luis Vicario on 26/4/18.
//  Copyright © 2018 Leonardo Luis Vicario. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class AuthService {
    
    static let instance = AuthService()
    
    let defaults = UserDefaults.standard
    
    var isLoggedIn : Bool {
        get {
            return defaults.bool(forKey: Logged_In_Key)
        }
        set {
            defaults.set(newValue, forKey: Logged_In_Key)
        }
    }
    var authToken : String {
        get {
            return defaults.value(forKey: Token_Key) as? String ?? ""
        }
        set {
            defaults.set(newValue, forKey: Token_Key)
        }
    }
    var userEmail : String {
        get {
            return defaults.value(forKey: User_Email) as? String ?? ""
        }
        set {
            defaults.set(newValue, forKey: User_Email)
        }
    }
    
    func registerUser(email : String, password : String, completion: @escaping CompletionHandler) {
        
        let lowerCaseEmail = email.lowercased()
        let body : [String : Any] = [
            "email" : lowerCaseEmail,
            "password" : password
        ]
        
        Alamofire.request(Register_Url, method: .post, parameters: body, encoding: JSONEncoding.default, headers: Header).responseString { (response) in
            
            if response.result.error == nil {
                completion(true)
            } else {
                completion(false)
                debugPrint(response.result.error as Any)
            }
        }
    }
    
    func loginUser(email : String, password : String, completion : @escaping CompletionHandler) {
        
        let lowerCaseEmail = email.lowercased()
        let body : [String : Any] = [
            "email" : lowerCaseEmail,
            "password" : password
        ]
        
        Alamofire.request(Login_Url, method: .post, parameters: body, encoding: JSONEncoding.default, headers: Header).responseJSON { (response) in
            
            if response.result.error == nil {
                // SWIFT WAY
                //                if let json = response.result.value as? Dictionary<String,Any> {
                //                    if let email = json["user"] as? String {
                //                        self.userEmail = email
                //                    }
                //                    if let token = json["token"] as? String {
                //                        self.authToken = token
                //                    }
                //                }
                // SWIFTYJSON kinda
                guard let data = response.data else {return}
                do {
                    let json = try JSON(data: data)
                    print("login_url try")
                    self.userEmail = json["user"].stringValue
                    print(self.userEmail)
                    self.authToken = json["token"].stringValue
                } catch {
                    print("login_url catch")
                    debugPrint(error)
                }
                self.isLoggedIn = true
                completion(true)
            } else {
                completion(false)
                debugPrint(response.result.error as Any)
            }
        }
        
    }
    
    func createUser(name: String, email: String, avatarName: String, avatarColor: String, completion: @escaping CompletionHandler) {
        
        let lowerCaseEmail = email.lowercased()
        let body : [String : Any] = [
            "name" : name,
            "email" : lowerCaseEmail,
            "avatarName" : avatarName,
            "avatarColor" : avatarColor
        ]
        
        Alamofire.request(User_Add_Url, method: .post, parameters: body, encoding: JSONEncoding.default, headers: Header_Bearer).responseJSON { (response) in
            if response.result.error == nil {
                guard let data = response.data else {return}
                self.setUserInfo(data: data)
                completion(true)
                
            } else {
                completion(false)
                debugPrint(response.result.error as Any)
            }
        }
        
        
    }
    
    func findUserByEmail(completion: @escaping CompletionHandler) {
        Alamofire.request("\(User_By_Email_Url)\(userEmail)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: Header_Bearer).responseJSON { (response) in
            print("findUserByEmail before if condition")
            if response.result.error == nil {
                print("User was found")
                guard let data = response.data else {return}
                self.setUserInfo(data: data)
                completion(true)
            } else {
                print("findUserByEmail error isn't nil")
                completion(false)
                debugPrint(response.result as Any)
            }
        }
    }
    
    func setUserInfo(data : Data) {
        print("Inside set user info")
        do {
            let json = try JSON(data: data)
            let id = json["_id"].stringValue
            let color = json["avatarColor"].stringValue
            let avatarName = json["avatarName"].stringValue
            let email = json["email"].stringValue
            let name = json["name"].stringValue
            
            UserDataService.instance.setUserData(id: id, color: color, avatarName: avatarName, email: email, name: name)
        } catch {
            debugPrint(error)
        }
    }
    
    
}

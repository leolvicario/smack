//
//  Constants.swift
//  Smack
//
//  Created by Leonardo Luis Vicario on 11/4/18.
//  Copyright © 2018 Leonardo Luis Vicario. All rights reserved.
//

import Foundation

// Typealias
typealias CompletionHandler = (_ Success: Bool) -> ()

// Url
let Base_url = "https://smackchatleolvicario.herokuapp.com/v1/"
let Register_Url = "\(Base_url)account/register"
let Login_Url = "\(Base_url)account/login"
let User_Add_Url = "\(Base_url)user/add"
let User_By_Email_Url = "\(Base_url)/user/byEmail/"
let Channel_Url = "\(Base_url)channel"
let Messages_Get_Url = "\(Base_url)message/byChannel/"

// Headers
let Header = [
    "Content-Type" : "application/json"
]

let Header_Bearer = [
    "Authorization":"Bearer \(AuthService.instance.authToken)",
    "Content-Type":"application/json"
]

// Segues
let To_Login = "toLogin"
let To_Create_Account = "toCreateAccount"
let Unwind = "unwindToChannel"
let To_Avatar_Picker = "toAvatarPicker"

// User Defaults
let Token_Key = "token"
let Logged_In_Key = "loggedIn"
let User_Email = "userEmail"

// Colors
let smackPurplePlaceholder = #colorLiteral(red: 0.3266413212, green: 0.4215201139, blue: 0.7752227187, alpha: 0.5032694945)

// Notification Constants
let Notif_User_Data_Did_Change = Notification.Name("notifUserDataChange")
let Notif_Channels_Loaded = Notification.Name("channelsLoaded")
let Notif_Channel_Selected = Notification.Name("channelSelected")

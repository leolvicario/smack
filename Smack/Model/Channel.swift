//
//  Channel.swift
//  Smack
//
//  Created by Leonardo Luis Vicario on 10/5/18.
//  Copyright © 2018 Leonardo Luis Vicario. All rights reserved.
//

import Foundation

struct Channel: Decodable {
    
    public private(set) var _id: String!
    public private(set) var name: String!
    public private(set) var description: String!
    public private(set) var __v: Int?
}
